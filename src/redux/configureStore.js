import {combineReducers, createStore, applyMiddleware} from 'redux';
import {Dishes} from "./dishes";
import {Comments} from "./comments";
import {Leaders} from "./leaders";
import {Promotions} from "./promotion";
import thunk from "redux-thunk";
import logger from 'redux-logger'
import {createForms} from 'react-redux-form';
import {InitialFeedback} from "./forms";
import {Feedback} from "./feedback";

export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            dishes: Dishes,
            comments: Comments,
            promotions: Promotions,
            leaders: Leaders,
            feedback: Feedback,
            ...createForms({
                feedback: InitialFeedback
            })
        }),
        applyMiddleware(thunk, logger)
    );

    return store;
}